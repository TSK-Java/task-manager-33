package ru.tsc.kirillov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectTaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectTaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.kirillov.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?WSDL", host, port, NAME);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IProjectTaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectBindTaskByIdResponse bindTaskToProjectId(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectBindTaskByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectUnbindTaskByIdResponse unbindTaskToProjectId(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUnbindTaskByIdRequest request
    );

}
