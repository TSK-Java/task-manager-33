package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final Integer index, @Nullable final Status status) {
        super(index);
        this.status = status;
    }

}
