package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByIdRequest extends AbstractIdRequest {

    public TaskShowByIdRequest(@Nullable final String id) {
        super(id);
    }

}
