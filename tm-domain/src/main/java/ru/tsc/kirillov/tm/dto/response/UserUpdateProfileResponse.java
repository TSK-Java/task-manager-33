package ru.tsc.kirillov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.User;

public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

}
