package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProjectRemoveByIdResponse extends AbstractProjectResponse {
}
