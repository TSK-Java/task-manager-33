package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class TaskChangeStatusByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(@Nullable final String id, @Nullable final Status status) {
        super(id);
        this.status = status;
    }

}
