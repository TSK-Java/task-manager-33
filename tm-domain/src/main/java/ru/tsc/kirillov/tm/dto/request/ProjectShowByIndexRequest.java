package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIndexRequest extends AbstractIndexRequest {

    public ProjectShowByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}
