package ru.tsc.kirillov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ServerAboutRequest;
import ru.tsc.kirillov.tm.dto.request.ServerVersionRequest;
import ru.tsc.kirillov.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.kirillov.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.kirillov.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?WSDL", host, port, NAME);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ApplicationAboutResponse getAbout(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerAboutRequest request
    );

    @NotNull
    @WebMethod
    ApplicationVersionResponse getVersion(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerVersionRequest request
    );

}
