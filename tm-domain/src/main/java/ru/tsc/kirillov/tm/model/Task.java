package ru.tsc.kirillov.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.model.IWBS;
import ru.tsc.kirillov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date created = new Date();

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateBegin;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateEnd;

    @Nullable
    private String projectId = null;

    public Task(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
