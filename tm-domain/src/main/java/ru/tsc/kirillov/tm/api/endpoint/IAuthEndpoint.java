package ru.tsc.kirillov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.request.UserLogoutRequest;
import ru.tsc.kirillov.tm.dto.request.UserProfileRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.dto.response.UserLogoutResponse;
import ru.tsc.kirillov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.kirillov.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?WSDL", host, port, NAME);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request
    );

}
