package ru.tsc.kirillov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.kirillov.tm.api.service.IServiceLocator;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;
import ru.tsc.kirillov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.kirillov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJaxbLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJaxbSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXmlLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXmlSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }
    
}
