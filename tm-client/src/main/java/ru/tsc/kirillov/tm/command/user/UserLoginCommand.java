package ru.tsc.kirillov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выполнение авторизации.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Авторизация]");
        System.out.println("Введите логин");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse response = getServiceLocator().getAuthEndpointClient().login(request);
        if (!response.getSuccess()) throw new Exception(response.getMessage());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
