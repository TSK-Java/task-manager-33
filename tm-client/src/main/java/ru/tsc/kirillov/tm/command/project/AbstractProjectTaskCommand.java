package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.client.IProjectTaskEndpointClient;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @NotNull
    public IProjectTaskEndpointClient getProjectTaskEndpoint() {
        return getServiceLocator().getProjectTaskEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
