package ru.tsc.kirillov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.client.IUserEndpointClient;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfileUser(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse viewProfileUser(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

}
