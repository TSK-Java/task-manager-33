package ru.tsc.kirillov.tm.api.client;

import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
