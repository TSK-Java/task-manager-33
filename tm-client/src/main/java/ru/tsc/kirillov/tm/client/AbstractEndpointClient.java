package ru.tsc.kirillov.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.client.IEndpointClient;
import ru.tsc.kirillov.tm.dto.response.ApplicationErrorResponse;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        host = client.host;
        port = client.port;
        socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, @NotNull Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @SneakyThrows
    public Socket disconnect() {
        socket.close();
        return socket;
    }

}
