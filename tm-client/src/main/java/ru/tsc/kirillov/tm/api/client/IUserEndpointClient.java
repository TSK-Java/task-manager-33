package ru.tsc.kirillov.tm.api.client;

import ru.tsc.kirillov.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}
